precision highp float;

uniform sampler2D tex0;
uniform float factor1;
uniform float factor2;

varying vec2 vTexCoord;

float random(vec2 uv) {
   const float PHI = 1.61803398874989484820459;
   return fract(sin(distance(uv*PHI,uv))*uv.x);
}

void main() {
    float noise = random(vTexCoord * (5000.0 + fract(factor1)));
    vec4 color = texture2D(tex0, vTexCoord);
    vec3 result = clamp(color.rgb + (noise-0.5) * factor2, vec3(0.0), vec3(1.0));
    gl_FragColor = vec4(result, 1.0) * color.a;
}