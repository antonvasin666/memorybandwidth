precision highp float;

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform float scaleFactor;

varying vec2 vTexCoord;

float color2displacement(float c) {
    float displ = c * 255.0;
    float delta = displ - 127.5;
    return sign(delta) * clamp(abs(delta) - 7.5, 0.0, 1000000.0);
}
void main() {
    vec2 displ = texture2D(tex1, vTexCoord).rg;
    vec2 displacement = vec2(color2displacement(displ.r), color2displacement(displ.g));
    displacement = displacement * scaleFactor;
    gl_FragColor = texture2D(tex0, vTexCoord + displacement);
}