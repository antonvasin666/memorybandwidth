package com.example.memorybandwidth;

import android.opengl.GLES30;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class Square {
    static float[] squareCoords = {
            0.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
    };
    static short[] indices = {0, 1, 2, 0, 2, 3};

    private final FloatBuffer vertexBuffer;
    private final ShortBuffer indicesBuffer;

    public Square() {
        ByteBuffer bb = ByteBuffer.allocateDirect(squareCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(squareCoords);
        vertexBuffer.position(0);

        ByteBuffer dlb = ByteBuffer.allocateDirect(indices.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        indicesBuffer = dlb.asShortBuffer();
        indicesBuffer.put(indices);
        indicesBuffer.position(0);

    }

    public void draw(ShaderProgram program, Texture2D mainTex) throws Exception {

        try (Texture2D.Texture2DBinder ignored = mainTex.bind(0)) {
            try (ShaderProgram.ShaderActivator sa = program.activate()) {

                int positionHandle = program.getAttribute("aPosition");
                int tex0Handler = program.getUniform("tex0");

                GLES30.glEnableVertexAttribArray(positionHandle);
                GLE.check();

                GLES30.glVertexAttribPointer(positionHandle, 3, GLES30.GL_FLOAT, false, 3 * 4, vertexBuffer);
                GLE.check();

                GLES30.glUniform1i(tex0Handler, 0);
                GLE.check();

                GLES30.glDrawElements(GLES30.GL_TRIANGLES, 3 * 2, GLES30.GL_UNSIGNED_SHORT, indicesBuffer);
                GLE.check();

                GLES30.glDisableVertexAttribArray(positionHandle);
                GLE.check();
            }
        }
    }

    public void draw(ShaderProgram program, Texture2D mainTex, Texture2D displace, float scaleFactor) throws Exception {

        try (Texture2D.Texture2DBinder ignored = mainTex.bind(0)) {
            try (Texture2D.Texture2DBinder ignored1 = displace.bind(1)) {
                try (ShaderProgram.ShaderActivator sa = program.activate()) {

                    int positionHandle = program.getAttribute("aPosition");
                    int tex0Handler = program.getUniform("tex0");
                    int tex1Handler = program.getUniform("tex1");
                    int scaleHandler = program.getUniform("scaleFactor");

                    GLES30.glEnableVertexAttribArray(positionHandle);
                    GLE.check();

                    GLES30.glVertexAttribPointer(positionHandle, 3, GLES30.GL_FLOAT, false, 3 * 4, vertexBuffer);
                    GLE.check();

                    GLES30.glUniform1i(tex0Handler, 0);
                    GLE.check();
                    GLES30.glUniform1i(tex1Handler, 1);
                    GLE.check();
                    GLES30.glUniform1f(scaleHandler, scaleFactor);
                    GLE.check();

                    GLES30.glDrawElements(GLES30.GL_TRIANGLES, 3 * 2, GLES30.GL_UNSIGNED_SHORT, indicesBuffer);
                    GLE.check();

                    GLES30.glDisableVertexAttribArray(positionHandle);
                    GLE.check();
                }
            }
        }
    }

    public void draw(ShaderProgram program, Texture2D mainTex, float factor1, float factor2) throws Exception {

        try (Texture2D.Texture2DBinder ignored = mainTex.bind(0)) {
            try (ShaderProgram.ShaderActivator sa = program.activate()) {

                int positionHandle = program.getAttribute("aPosition");
                int tex0Handler = program.getUniform("tex0");
                int factor1Handler = program.getUniform("factor1");
                int factor2Handler = program.getUniform("factor2");

                GLES30.glEnableVertexAttribArray(positionHandle);
                GLE.check();

                GLES30.glVertexAttribPointer(positionHandle, 3, GLES30.GL_FLOAT, false, 3 * 4, vertexBuffer);
                GLE.check();

                GLES30.glUniform1i(tex0Handler, 0);
                GLE.check();
                GLES30.glUniform1f(factor1Handler, factor1);
                GLE.check();
                GLES30.glUniform1f(factor2Handler, factor2);
                GLE.check();

                GLES30.glDrawElements(GLES30.GL_TRIANGLES, 3 * 2, GLES30.GL_UNSIGNED_SHORT, indicesBuffer);
                GLE.check();

                GLES30.glDisableVertexAttribArray(positionHandle);
                GLE.check();
            }
        }
    }
}