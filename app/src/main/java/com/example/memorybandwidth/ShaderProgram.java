package com.example.memorybandwidth;

import android.opengl.GLES30;
import android.util.Log;

import java.util.TreeMap;

public class ShaderProgram implements AutoCloseable {
    private final Shader mVertex;
    private final Shader mFragment;
    private final int mProgram;
    private final TreeMap<String, Integer> mUniforms = new TreeMap<>();
    private final TreeMap<String, Integer> mAttributes = new TreeMap<>();
    private boolean mDisposed = false;

    public ShaderProgram(Shader vertex, Shader fragment) {
        mVertex = vertex;
        mFragment = fragment;

        mProgram = GLES30.glCreateProgram();
        GLE.check();

        GLES30.glAttachShader(mProgram, mVertex.getShaderHandle());
        GLE.check();

        GLES30.glAttachShader(mProgram, mFragment.getShaderHandle());
        GLE.check();

        GLES30.glLinkProgram(mProgram);
        GLE.check();

        GLES30.glValidateProgram(mProgram);
        Log.i("OpenGL", GLES30.glGetProgramInfoLog(mProgram));
    }

    int getProgram() {
        return mProgram;
    }

    int getUniform(String name) {
        Integer loc = mUniforms.get(name);
        if (loc == null) {
            int val = GLES30.glGetUniformLocation(mProgram, name);
            GLE.check();
            if (val == -1)
                throw new RuntimeException("no such uniform");

            mUniforms.put(name, val);
            return val;
        }

        return loc;
    }

    int getAttribute(String name) {
        Integer loc = mAttributes.get(name);
        if (loc == null) {
            int val = GLES30.glGetAttribLocation(mProgram, name);
            GLE.check();
            if (val == -1)
                throw new RuntimeException("no such attribute");

            mAttributes.put(name, val);
            return val;
        }

        return loc;
    }

    public ShaderActivator activate() {
        return new ShaderActivator(this);
    }

    @Override
    public void close() {
        if (mDisposed)
            return;

        GLES30.glDeleteProgram(mProgram);
        GLE.check();

        mVertex.close();
        mFragment.close();

        mDisposed = true;
    }

    @Override
    protected void finalize() {
        close();
    }

    public static class ShaderActivator implements AutoCloseable {
        private final int mLastShader;

        private ShaderActivator(ShaderProgram program) {
            int[] temp = {0};
            GLES30.glGetIntegerv(GLES30.GL_CURRENT_PROGRAM, temp, 0);
            GLE.check();
            mLastShader = temp[0];

            GLES30.glUseProgram(program.getProgram());
            GLE.check();
        }

        @Override
        public void close() throws Exception {
            GLES30.glUseProgram(mLastShader);
            GLE.check();
        }
    }
}
