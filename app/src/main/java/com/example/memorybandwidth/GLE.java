package com.example.memorybandwidth;

import android.opengl.GLES30;
import android.opengl.GLU;

public class GLE {
    public static void check() {
        int error = GLES30.glGetError();
        if (error != GLES30.GL_NO_ERROR) {
            throw new RuntimeException(GLU.gluErrorString(error));
        }
    }
}
