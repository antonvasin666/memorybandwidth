package com.example.memorybandwidth;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Random;

public class TextureFactory {
    static Texture2D createGradient(int width, int height)
    {
        final int bytes = width * height * 3;
        byte[] data = new byte[bytes];

        for (int y = 0; y < height; ++y) {
            final int stride = y * width * 3;
            for (int x = 0; x < width; ++x) {
                final int idx = stride + x * 3;
                byte r = (byte) (255.0f * (float) x / (float) width);
                byte g = (byte) (255.0f * (float) y / (float) height);
                byte b = (byte) (255.0f * (float) (x + y) / (float) (width + height));
                data[idx] = r;
                data[idx + 1] = g;
                data[idx + 2] = b;
            }
        }

        ByteBuffer imageData = ByteBuffer.allocateDirect(bytes);
        imageData.order(ByteOrder.nativeOrder());
        imageData.put(data);
        imageData.position(0);

        Texture2D result = new Texture2D();
        result.upload(width, height, imageData);

        return result;
    }

    static Texture2D createRandom(int width, int height, int seed)
    {
        final int bytes = width * height * 3;
        byte[] data = new byte[bytes];
        Random rand = new Random(seed);
        rand.nextBytes(data);

        ByteBuffer imageData = ByteBuffer.allocateDirect(bytes);
        imageData.order(ByteOrder.nativeOrder());
        imageData.put(data);
        imageData.position(0);

        Texture2D result = new Texture2D();
        result.upload(width, height, imageData);

        return result;
    }
}
