package com.example.memorybandwidth;

import android.content.Context;
import android.opengl.GLSurfaceView;

class MyGLSurfaceView extends GLSurfaceView {

    public MyGLSurfaceView(Context context) {
        super(context);

        setEGLContextClientVersion(3);
        MyGLRenderer renderer = new MyGLRenderer(context);
        setRenderer(renderer);
    }
}