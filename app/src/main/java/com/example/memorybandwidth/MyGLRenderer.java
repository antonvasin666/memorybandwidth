package com.example.memorybandwidth;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.opengl.GLSurfaceView;

import java.io.IOException;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MyGLRenderer implements GLSurfaceView.Renderer {
    final Context mContext;
    ShaderProgram mBlitProgram = null;
    ShaderProgram mFilterProgram = null;
    Texture2D mDisplace = null;
    Texture2D mGradient = null;
    Texture2D mColorBuffer0 = null;
    Texture2D mColorBuffer1 = null;
    FrameBuffer mFrameBuffer = null;
    Square mSquare = null;

    MyGLRenderer(Context ctx) {
        mContext = ctx;
    }

    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLE.check();

        try {
            Shader simpleVertex = new Shader(mContext, "simple.vert", GLES30.GL_VERTEX_SHADER);
            Shader simpleFrag = new Shader(mContext, "simple.frag", GLES30.GL_FRAGMENT_SHADER);
            Shader effectFrag = new Shader(mContext, "noise.frag", GLES30.GL_FRAGMENT_SHADER);

            mBlitProgram = new ShaderProgram(simpleVertex, simpleFrag);
            mFilterProgram = new ShaderProgram(simpleVertex, effectFrag);

            final int kWidth = 8192;
            final int kHeight = 4096;

            mGradient = TextureFactory.createGradient(kWidth, kHeight);
            mColorBuffer0 = new Texture2D(kWidth, kHeight);
            mColorBuffer1 = new Texture2D(kWidth, kHeight);
            //mDisplace = TextureFactory.createRandom(kWidth, kHeight, 42);

            mFrameBuffer = new FrameBuffer();
            mSquare = new Square();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onDrawFrame(GL10 unused) {
        try {
            for (int i = 1; i <= 1; ++i) {
                try (FrameBuffer.FramebufferBinder fbb = mFrameBuffer.bind(mColorBuffer1)) {
                    mSquare.draw(mFilterProgram, i == 1 ? mGradient : mColorBuffer0, (float)i / 10.0f, (float)i);

                    Texture2D temp = mColorBuffer0;
                    mColorBuffer0 = mColorBuffer1;
                    mColorBuffer1 = temp;
                }
            }
            mSquare.draw(mBlitProgram, mColorBuffer1);
//            mSquare.draw(mFilterProgram, mGradient, 1.0f, 2.0f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        GLE.check();
    }
}